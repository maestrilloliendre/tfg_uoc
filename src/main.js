import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '@/App.vue'
import VueCarousel from 'vue-carousel'
import firebase from 'firebase'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

import routes from '@/routes'

import EventBus from '@/plugins/event-bus'

// Librerías a utilizar
Vue.use(VueRouter)
Vue.use(EventBus)
Vue.use(VueCarousel)
Vue.use(Buefy)

// Declaramos una instacia de Vue Router
const router = new VueRouter({ routes })

// Guardamos en la variable la configuración de Firebase
const config = {
  apiKey: 'AIzaSyDf9ivIVMAsdkEN30e8GAcIyVFghsgy5IE',
  authDomain: 'menureverde.firebaseapp.com',
  databaseURL: 'https://menureverde.firebaseio.com',
  projectId: 'menureverde',
  storageBucket: 'menureverde.appspot.com',
  messagingSenderId: '1071247826839'
}

// Inicializamos la instancia de Firebase
firebase.initializeApp(config)

// Autenticacion con Firebase
export const provider = new firebase.auth.GoogleAuthProvider()

// Obtener una instancia a la base de datos
export const database = firebase.database()

let firebaseAppDefined = false
setInterval(() => {
  if (!firebaseAppDefined) {
    if (firebase.app()) {
      new Vue({
        el: '#app',
        render: h => h(App),
        router
      })
      firebaseAppDefined = true
    }
  }
}, 100)
