import Search from '@/components/Search.vue'
import RecipeDetail from '@/components/RecipeDetail.vue'
import Planner from '@/components/Planner.vue'
import Misrecetas from '@/components/Misrecetas.vue'

const routes = [
  {
    path: '/',
    component: Search,
    name: 'search'
  },
  {
    path: '/misrecetas',
    component: Misrecetas,
    name: 'misrecetas'
  },
  {
    path: '/recipe/:r',
    component: RecipeDetail,
    name: 'recipe'
  },
  {
    path: '/recipefavs/:r',
    component: RecipeDetail,
    name: 'recipefavs'
  },
  { path: '/planner',
    component: Planner,
    name: 'planner'
  }
]

export default routes
