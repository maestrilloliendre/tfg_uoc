// Comunicación entre componentes
// creo un objeto vacío
const eventBus = {}

// Funcion install que Vue va a utilizar para declararlo y utilizarlo en todos los componentes
eventBus.install = function (Vue) {
  Vue.prototype.$bus = new Vue()
}

export default eventBus
