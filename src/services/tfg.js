// Servicio que utilizaremos para interactuar con la API de recetas

import trae from 'trae' // libreria para Fetch API (consultas api rest)
import configService from './config'

// Creamos una instancia de trae
const tfgService = trae.create({
  baseUrl: configService.apiUrl
})

export default tfgService
