/*
   Servicio que nos permite interactuar con la API, buscando recetas
   y obteniendo información de las mismas
*/
import tfgService from './tfg'

const recipeService = {}

recipeService.search = function (q) {
  // Parámetros requeridos de la API
  const app_id = '4485828c'
  const app_key = '73e1c2cd806e015b10451122ef261e2a'
  const health = 'vegetarian'

  return tfgService.get('/search', {
    params: { q, app_id, app_key, health }
  })
    .then(res => res.data)
}

recipeService.getByUri = function (r) {
  const app_id = '4485828c'
  const app_key = '73e1c2cd806e015b10451122ef261e2a'
  return tfgService.get('/search', {
    params: { app_id, app_key, r }
  })
    .then(res => res.data)
}

export default recipeService
